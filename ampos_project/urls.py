"""af URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from restaurant import views

urlpatterns = [
    path('api/1.0/menuitems', views.menuitems),
    path('api/1.0/menuitems/<int:id>', views.item),
    path('api/1.0/bills', views.billlist),
    path('api/1.0/bills/<int:id>', views.bills),
    path('api/1.0/bills/<int:id>/items', views.bill_item_list),
    path('api/1.0/bills/<int:bill_id>/items/<int:id>', views.bill_items),
    path('api/1.0/checkout', views.checkout),
]
