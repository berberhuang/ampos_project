import json
from django.test import TestCase
from ..models import MenuItem, Bill, BillOrderItem

class MenuItemViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        for num in range(1,100):
            MenuItem.objects.create(name='Item {}'.format(num), description='', image='', price=25, additional_detail='')

    def test_get_menuitem_list_page_first(self): 
        resp = self.client.get('/api/1.0/menuitems', {'page':1}) 

        self.assertEqual(resp.status_code, 200)
        content = json.loads(resp.content)
        self.assertEqual(len(content['data']), 10)
        self.assertEqual(content['meta']['count'], 99)

        self.assertEqual(content['links']['self'].split('=')[-1], '1')
        self.assertEqual(content['links']['last'].split('=')[-1], '10')
        self.assertEqual(content['links']['next'].split('=')[-1], '2')

    def test_get_menuitem_list_page2(self): 
        resp = self.client.get('/api/1.0/menuitems', {'page':2}) 

        self.assertEqual(resp.status_code, 200)
        content = json.loads(resp.content)
        self.assertEqual(len(content['data']), 10)
        self.assertEqual(content['meta']['count'], 99)

        self.assertEqual(content['links']['self'].split('=')[-1], '2')
        self.assertEqual(content['links']['last'].split('=')[-1], '10')
        self.assertEqual(content['links']['next'].split('=')[-1], '3')
        self.assertEqual(content['links']['prev'].split('=')[-1], '1')

    def test_get_menuitem_list_page_last(self): 
        resp = self.client.get('/api/1.0/menuitems', {'page':10}) 

        self.assertEqual(resp.status_code, 200)
        content = json.loads(resp.content)
        self.assertEqual(len(content['data']), 9)
        self.assertEqual(content['meta']['count'], 99)

        self.assertEqual(content['links']['self'].split('=')[-1], '10')
        self.assertEqual(content['links']['last'].split('=')[-1], '10')
        self.assertEqual(content['links']['prev'].split('=')[-1], '9')

    def test_get_menuitem_list_nonexist_page(self): 
        resp = self.client.get('/api/1.0/menuitems', {'page':20}) 

        self.assertEqual(resp.status_code, 200)
        content = json.loads(resp.content)
        self.assertEqual(len(content['data']), 0)
        self.assertEqual(content['meta']['count'], 99)

        self.assertEqual(content['links']['self'].split('=')[-1], '20')
        self.assertEqual(content['links']['last'].split('=')[-1], '10')
        
    def test_get_menuitem_list_by_name(self): 
        resp = self.client.get('/api/1.0/menuitems', {'keyword':'50'}) 

        self.assertEqual(resp.status_code, 200)
        content = json.loads(resp.content)
        self.assertEqual(len(content['data']), 1)
        self.assertEqual(content['data'][0]['name'], 'Item 50')
        self.assertEqual(content['meta']['count'], 1)

        self.assertEqual(content['links']['self'].split('=')[-1], '1')
        self.assertEqual(content['links']['last'].split('=')[-1], '1')

    def test_create_menuitem(self): 
        resp = self.client.post('/api/1.0/menuitems', {
            'name': 'TestName',
            'description': 'test description',
            'image':'url',
            'price':30,
            'additionalDetail':'pizza',
        }) 
        self.assertEqual(resp.status_code, 200)
        
        content = json.loads(resp.content)
        record = MenuItem.objects.get(id=content['id'])
        self.assertEqual(record.name, 'TestName')
        self.assertEqual(record.description, 'test description')
        self.assertEqual(record.image, 'url')
        self.assertEqual(record.price, 30)
        self.assertEqual(record.additional_detail, 'pizza')

    def test_get_menuitem(self): 
        resp = self.client.get('/api/1.0/menuitems/5') 
        self.assertEqual(resp.status_code, 200)
        content = json.loads(resp.content)
        self.assertEqual(content['name'], 'Item 5')
    
    def test_get_nonexist_menuitem(self): 
        resp = self.client.get('/api/1.0/menuitems/100') 
        self.assertEqual(resp.status_code, 404)

    def test_delete_menuitem(self):
        resp = self.client.delete('/api/1.0/menuitems/1')
        self.assertEqual(resp.status_code, 204)

    def test_delete_nonexist_menuitem(self):
        resp = self.client.delete('/api/1.0/menuitems/1000')
        self.assertEqual(resp.status_code, 404)

    def test_patch_menuitem(self):
        target_name = 'modified name'
        data = json.dumps({'name': target_name})
        resp = self.client.patch('/api/1.0/menuitems/2', data, content_type='application/json')
        self.assertEqual(resp.status_code, 204)
        
        self.assertEqual(MenuItem.objects.get(id=2).name, target_name)

    def test_patch_nonexist_menuitem(self):
        data = json.dumps({'name':'modified name'})
        resp = self.client.patch('/api/1.0/menuitems/1000', data, content_type='application/json')
        self.assertEqual(resp.status_code, 404)


class BillViewTest(TestCase):
    @classmethod
    def setupTestData(cls):
        for num in range(1,20):
            MenuItem.objects.create(name='Item {}'.format(num), price=25)

        bill1 = Bill.objects.create()
        BillOrderItem.objects.create(bill_id=bill1.id, menu_item_id=1, qty=2)
        BillOrderItem.objects.create(bill_id=bill1.id, menu_item_id=2, qty=3)

        bill2 = Bill.objects.create()
        BillOrderItem.objects.create(bill_id=bill2.id, menu_item_id=5, qty=1)
        BillOrderItem.objects.create(bill_id=bill2.id, menu_item_id=6, qty=1)
        BillOrderItem.objects.create(bill_id=bill2.id, menu_item_id=7, qty=1)

    def setUp(self):
        BillViewTest.setupTestData()

    def test_get_bill(self): 
        resp = self.client.get('/api/1.0/bills/1') 

        self.assertEqual(resp.status_code, 200)
        
        content = json.loads(resp.content)
        self.assertEqual(len(content['orderItems']), 2)
        self.assertEqual(content['isPaid'], False)
        self.assertEqual(content['price'], 125)

    def test_create_bill(self): 
        resp = self.client.post('/api/1.0/bills') 

        self.assertEqual(resp.status_code, 200)
        
        content = json.loads(resp.content)
        self.assertEqual(len(content['orderItems']), 0)
        self.assertEqual(content['isPaid'], False)
        self.assertEqual(content['price'], 0)

    def test_add_billItem(self): 
        resp = self.client.post('/api/1.0/bills/1/items', {'menu_item_id':1, 'qty':2}) 

        self.assertEqual(resp.status_code, 200)
        
        content = json.loads(resp.content)
        self.assertEqual(len(content['orderItems']), 3)
        self.assertEqual(content['isPaid'], False)
        self.assertEqual(content['price'], 175)

    def test_add_billItem(self): 
        resp = self.client.delete('/api/1.0/bills/1/items/1') 

        self.assertEqual(resp.status_code, 200)

        content = json.loads(resp.content)
        self.assertEqual(len(content['orderItems']), 1)
        self.assertEqual(content['isPaid'], False)
        self.assertEqual(content['price'], 75)

    def test_checkout_bill(self):
        resp = self.client.post('/api/1.0/checkout',{"id":1})

        self.assertEqual(resp.status_code, 200)

        content = json.loads(resp.content)
        self.assertEqual(len(content['orderItems']), 2)
        self.assertEqual(content['isPaid'], True)
        self.assertEqual(content['price'], 125)