from django.test import TestCase
from ..models import MenuItem, Bill, BillOrderItem

class MenuItemModelTestClass(TestCase):

    @classmethod
    def setupTestData(cls):
        for num in range(20):
            MenuItem.objects.create(name='Item {}'.format(num), price=25)

    def setUp(self):
        MenuItemModelTestClass.setupTestData()

    def test_keyword(self):
        r = MenuItem.objects.keyword('15')
        self.assertEqual(len(r), 1)
        self.assertEqual(r[0].name, 'Item 15')

class BillModelTestClass(TestCase):

    @classmethod
    def setupTestData(cls):
        for num in range(1,20):
            MenuItem.objects.create(name='Item {}'.format(num), price=25)

        bill1 = Bill.objects.create()
        BillOrderItem.objects.create(bill_id=bill1.id, menu_item_id=1, qty=2)
        BillOrderItem.objects.create(bill_id=bill1.id, menu_item_id=2, qty=3)

        bill2 = Bill.objects.create()
        BillOrderItem.objects.create(bill_id=bill2.id, menu_item_id=5, qty=1)
        BillOrderItem.objects.create(bill_id=bill2.id, menu_item_id=6, qty=1)
        BillOrderItem.objects.create(bill_id=bill2.id, menu_item_id=7, qty=1)

    def setUp(self):
        BillModelTestClass.setupTestData()

    def test_get_bill(self):
        r = Bill.objects.get_order_item_list(1)
        self.assertEqual(len(r['orderItems']), 2)
        self.assertEqual(r['isPaid'], False)
        self.assertEqual(r['price'], 125)

    def test_add_bill_item(self):
        r = Bill.objects.add_order_item(1, 4, 1)
        self.assertEqual(len(r['orderItems']), 3)
        self.assertEqual(r['isPaid'], False)
        self.assertEqual(r['price'], 150)

    def test_delete_bill_item(self):
        r = Bill.objects.delete_order_item(1)
        self.assertEqual(len(r['orderItems']), 1)
        self.assertEqual(r['isPaid'], False)
        self.assertEqual(r['price'], 75)

    def test_modify_bill_item(self):
        r = Bill.objects.modiy_order_item_qty(1, 4)
        self.assertEqual(len(r['orderItems']), 2)
        self.assertEqual(r['isPaid'], False)
        self.assertEqual(r['price'], 175)

    def test_checkout_bill(self):
        Bill.objects.checkout(1)
        r = Bill.objects.get_order_item_list(1)
        self.assertEqual(len(r['orderItems']), 2)
        self.assertEqual(r['isPaid'], True)
        self.assertEqual(r['price'], 125)
