import json
from django.core.paginator import Paginator
from django.http import HttpResponse, QueryDict
from django.views.decorators.http import require_http_methods
from .models import MenuItem, Bill


@require_http_methods(['GET', 'POST'])
def menuitems(request):
    if request.method == 'GET':
        if 'page' in request.GET:
            page_idx = int(request.GET['page'])
        else:
            page_idx = 1

        keyword = None
        if 'keyword' in request.GET:
            keyword = request.GET['keyword']

        if keyword is not None:
            page = Paginator(MenuItem.objects.keyword(keyword), 10)
        else:
            page = Paginator(MenuItem.objects.order_by('id').all(), 10)

        if 1 <= page_idx <= page.num_pages:
            data = [m.getDict() for m in page.get_page(page_idx)]
        else:
            data = []

        r = {
            'data': data,
            'meta': {
                'count': page.count
            },
            "links": {
                "self": "/api/1.0/menuitems?page={}".format(page_idx),
                "last": "/api/1.0/menuitems?page={}".format(page.num_pages)
            }
        }
        if page_idx > 1:
            r['links']['prev'] = "/api/1.0/menuitems?page={}".format(
                page_idx-1)
        if page_idx < page.num_pages:
            r['links']['next'] = "/api/1.0/menuitems?page={}".format(
                page_idx+1)

        return HttpResponse(json.dumps(r), content_type='application/json')
    elif request.method == 'POST':
        if 'name' not in request.POST or 'description' not in request.POST or 'image' not in request.POST or 'price' not in request.POST or 'additionalDetail' not in request.POST:
            resp = HttpResponse()
            resp.status_code = 400
            return resp

        m = MenuItem.objects.create(
            name=request.POST['name'],
            description=request.POST['description'],
            image=request.POST['image'],
            price=request.POST['price'],
            additional_detail=request.POST['additionalDetail']
        )
        return HttpResponse(json.dumps(m.getDict()), content_type='application/json')


@require_http_methods(['GET', 'PATCH', 'DELETE'])
def item(request, id):
    try:
        if request.method == 'GET':
            m = MenuItem.objects.get(id=id)
            return HttpResponse(json.dumps(m.getDict()), content_type='application/json')
        elif request.method == 'DELETE':
            MenuItem.objects.get(id=id).delete()
        elif request.method == 'PATCH':
            m = MenuItem.objects.get(id=id)

            data = json.loads(request.body)
            m.name = data['name']
            m.save()
        resp = HttpResponse()
        resp.status_code = 204
        return resp
    except MenuItem.DoesNotExist:
        resp = HttpResponse()
        resp.status_code = 404
        return resp


@require_http_methods(['POST'])
def billlist(request):
    bill = Bill.objects.create()
    return HttpResponse(json.dumps(bill.getDict()), content_type='application/json')


@require_http_methods(['GET'])
def bills(request, id):
    try:
        r = Bill.objects.get_order_item_list(id=id)
        return HttpResponse(json.dumps(r), content_type='application/json')
    except Bill.DoesNotExist:
        resp = HttpResponse()
        resp.status_code = 404
        return resp


@require_http_methods(['POST'])
def bill_item_list(request, id):
    try:
        r = Bill.objects.add_order_item(
            bill_id=id, menu_item_id=request.POST['menu_item_id'], qty=request.POST['qty'])
        return HttpResponse(json.dumps(r), content_type='application/json')
    except Bill.DoesNotExist:
        resp = HttpResponse()
        resp.status_code = 404
        return resp


@require_http_methods(['DELETE', 'PATCH'])
def bill_items(request, bill_id, id):
    try:
        if request.method == 'DELETE':
            r = Bill.objects.delete_order_item(order_id=id)
        elif request.method == 'PATCH':
            if 'qty' not in request.POST:
                resp = HttpResponse()
                resp.status_code = 400
                return resp

            r = Bill.objects.modiy_order_item_qty(id, int(request.POST['qty']))
        return HttpResponse(json.dumps(r), content_type='application/json')
    except Bill.DoesNotExist:
        resp = HttpResponse()
        resp.status_code = 404
        return resp


@require_http_methods(['POST'])
def checkout(request):
    if 'id' not in request.POST:
        resp = HttpResponse()
        resp.status_code = 400
        return resp

    try:
        r = Bill.objects.checkout(int(request.POST['id']))
        return HttpResponse(json.dumps(r), content_type='application/json')
    except Bill.DoesNotExist:
        resp = HttpResponse()
        resp.status_code = 404
        return resp
