from django.db import models
from django.db.models import Q


class MenuItemQuerySet(models.QuerySet):
    def keyword(self, keyword):
        return self.filter(Q(name__contains=keyword) | Q(description__contains=keyword) | Q(additional_detail__contains=keyword)).order_by('id')


class MenuItem(models.Model):
    name = models.CharField(max_length=60)
    description = models.CharField(max_length=300, blank=True, null=True)
    image = models.CharField(max_length=150, blank=True, null=True)
    price = models.IntegerField()
    additional_detail = models.CharField(max_length=300, blank=True, null=True)

    objects = MenuItemQuerySet.as_manager()

    @classmethod
    def get_sentinel_instance(cls):
        return MenuItem.get_or_create(name="deleted", price=0)[0]

    def getDict(self):
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description,
            'image': self.image,
            'price': self.price,
            'additionalDetail': self.additional_detail,
        }


class BillQuerySet(models.QuerySet):
    def get_order_item_list(self, id):
        bill=self.get(id=id)
        order_items = BillOrderItem.objects.filter(bill=bill).values('qty','menu_item_id','menu_item__name', 'menu_item__price')
        return {
            'orderItems': [
                {
                    'name':item['menu_item__name'],
                    'price':item['menu_item__price'],
                    'qty':item['qty'],
                    'links':{
                        'menuItem':'/api/1.0/menuitems/{}'.format(item['menu_item_id'])
                    }
                } for item in order_items],
            'orderTime': bill.order_time.strftime('%m/%d/%Y'),
            'price': BillQuerySet.get_total_price(order_items),
            'isPaid': bill.is_paid,
        }

    def add_order_item(self, bill_id, menu_item_id, qty):
        BillOrderItem.objects.create(bill_id=bill_id, menu_item_id=menu_item_id, qty=qty)
        return self.get_order_item_list(bill_id)

    def delete_order_item(self, order_id):
        order=BillOrderItem.objects.get(id=order_id)
        order.delete()
        return self.get_order_item_list(order.bill_id)

    def modiy_order_item_qty(self, order_id, qty):
        order = BillOrderItem.objects.get(id=order_id)
        order.qty = qty
        order.save()
        return self.get_order_item_list(order.bill_id)

    def checkout(self, id):
        bill=self.get(id=id)
        bill.is_paid = True
        bill.save()
        return self.get_order_item_list(id)

    @classmethod
    def get_total_price(cls, order_items):
        price = 0
        for order in order_items:
            price += order['qty']*order['menu_item__price']
        return price

class Bill(models.Model):
    order_time = models.DateTimeField(auto_now_add=True)
    is_paid = models.BooleanField(default=False)

    objects = BillQuerySet.as_manager()

    def getDict(self):
        return {
            'id': self.id,
            'orderItems':[],
            'orderTime': self.order_time.strftime('%m/%d/%Y'),
            'isPaid': self.is_paid,
            'price':0,
        }


class BillOrderItem(models.Model):
    qty = models.IntegerField()

    bill = models.ForeignKey(Bill, on_delete=models.CASCADE)
    menu_item = models.ForeignKey(
        MenuItem, on_delete=models.SET(MenuItem.get_sentinel_instance))

    def getDict(self):
        return {
            'id': self.id,
            'menuItemId':self.menu_item_id,
            'qty': self.qty,
        }
